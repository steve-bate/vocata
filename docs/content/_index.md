+++
title = ""

# The homepage contents
[extra]
lead = '<p><img src="/vocata.svg" alt="Vocata" /></p><p>A vocabulary-agnostic <a href="https://activitypub.rocks/">ActivityPub</a> server for the <a href="https://fediverse.party/">Fediverse</a></p>'
url = "/docs/introduction/about/"
url_button = "About Vocata"
repo_version = "Codeberg"
repo_license = "GNU Lesser General Public License 3.0+"
repo_url = "https://codeberg.org/Vocata/vocata"

[[extra.list]]
title = "Graph-centric design"
content = 'Natively handles and stores the <b>social graph</b> as an <a href="https://www.w3.org/TR/rdf-concepts/">RDF graph</a> – read more in the <a href="/docs/introduction/graph/">graph introduction</a>'

[[extra.list]]
title = "Vocabulary-agnostic"
content = 'Stores and forwards activities without caring for the semantics of the transported objects – read more in the <a href="/docs/introduction/transport/">transport introduction</a>'

[[extra.list]]
title = "Standard compliance"
content = 'Adheres to <a href="https://www.w3.org/TR/activitypub/">ActivityPub</a> and related specifications – read more in the <a href="/docs/appendix/standards/">list of standards</a>'
+++
