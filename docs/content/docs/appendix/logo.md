+++
title = "The Vocata Logo"
description = "How the triangle logo came to be"
weight = 100
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

Vocata's logo reflects many of the concepts and assumptions
established above:

* It is based on the triangles, the arrow, and the colors
  of the [ActivityPub] logo
* Two more colors from a derived palette have been added, to
  highlight that the Fediverse should be more colorful than
  some semi-interoperable, yet somewhat zoned platforms
* Three triangles form a [Sierpinski triangle], which is a
  fractal, much like the structure of the [ActivityPub] social
  graph (global graph, instance sub-graph, and CBD transferred
  between them being three of its iterations)


[ActivityPub]: https://activitypub.rocks/
[Sierpinski triangle]: https://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle
